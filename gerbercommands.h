#ifndef GERBERCOMMANDS_H
#define GERBERCOMMANDS_H

#include "gerber.h"
#include <QtGlobal>
#include <QStringList>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QPointF>

class GerberFileReaderContext;
class GerberCommand;
class NullGerberCommand;

// TODO: 2 classes:
//  GerberFileReader + GerberCommandReader
//  GerberFileWriter + GerberCommandWriter

// In GerberFileReader: Commands as
//  D (?<ddata>[^*%]*)D0?(?<dcode>[0-9])\\*
//  G G(?<gcode>[0-9][0-9]?)(?<gdata>[^*%]*)\\*
//  M M(?<mcode>[0-9][0-9]?)(?<mdata>[^*%]*)\\*
//  A (?<adata>[^*%]*)D(?<acode>[1-9][0-9]+)\\*
//  X %(?<xcode>[A-Z][A-Z])(?<xdata>[^%]*\\*)%

// Pre/post
// (?<precode>[A-Za-z][0-9][0-9]?)?(?<fdata>[^*%]*)(?<postcode>[A-Za-z][0-9]+?)?\\*
class GerberCommandReader
{
public:
    GerberCommandReader(QChar family, const QString &code, const QString &format);
    virtual ~GerberCommandReader();

    QChar family() const;
    QString code() const;
    QRegularExpression format() const;
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context);
    QString errorString() const;
    GerberVersion version() const;

    virtual void updateContext(GerberFileReaderContext *context) const;

    virtual QStringList decompose() const;

    virtual GerberCommand *deserialise() const;

protected:
    void setErrorString(const QString &error);
    void clearErrorString();
    void setVersion(GerberVersion version);

private:
    const QChar m_family;
    const QString m_code;
    QRegularExpression m_format;
    QString m_errorString;
    GerberVersion m_version;
};

class G01_GerberCommandReader: public GerberCommandReader
{
public:
    G01_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G02_GerberCommandReader: public GerberCommandReader
{
public:
    G02_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G03_GerberCommandReader: public GerberCommandReader
{
public:
    G03_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G04_GerberCommandReader: public GerberCommandReader
{
public:
    G04_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_text;
};

class G36_GerberCommandReader: public GerberCommandReader
{
public:
    G36_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G37_GerberCommandReader: public GerberCommandReader
{
public:
    G37_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override; // TODO: rename to process?
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G54_GerberCommandReader: public GerberCommandReader
{
public:
    G54_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G55_GerberCommandReader: public GerberCommandReader
{
public:
    G55_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G70_GerberCommandReader: public GerberCommandReader
{
public:
    G70_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G71_GerberCommandReader: public GerberCommandReader
{
public:
    G71_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G74_GerberCommandReader: public GerberCommandReader
{
public:
    G74_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G75_GerberCommandReader: public GerberCommandReader
{
public:
    G75_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G90_GerberCommandReader: public GerberCommandReader
{
public:
    G90_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class G91_GerberCommandReader: public GerberCommandReader
{
public:
    G91_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_data;
};

class D01_GerberCommandReader: public GerberCommandReader
{
public:
    D01_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QPointF m_xy;
    QPointF m_ij;
};

class D02_GerberCommandReader: public GerberCommandReader
{
public:
    D02_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QPointF m_xy;
};

class D03_GerberCommandReader: public GerberCommandReader
{
public:
    D03_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QPointF m_xy;
};

// TODO: Coordinate data w/o Operation Code

class DNN_GerberCommandReader: public GerberCommandReader
{
public:
    DNN_GerberCommandReader();

    // GerberCommandReader interface
public:
    virtual bool parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context) override;
    virtual QStringList decompose() const override;
    virtual GerberCommand *deserialise() const override;

private:
    QString m_number;
};

class GerberCommand
{
public:
    GerberCommand() {}
    virtual ~GerberCommand() {}

    virtual int type() const { return -1; }
};

class NullGerberCommand: public GerberCommand
{
public:
    enum
    {
        Type = 0
    };

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }

};

class SelectInterpolationCommand: public GerberCommand
{
public:
    enum
    {
        Type = 1
    };


    GerberInterpolation interpolation = InvalidInterpolation;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class InterpolateCommand: public GerberCommand
{
public:
    enum
    {
        Type = 2
    };

    qreal xCoordinate = qSNaN();
    qreal yCoordinate = qSNaN();
    qreal xDistance = qSNaN();
    qreal yDistance = qSNaN();

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class MoveCommand: public GerberCommand
{
public:
    explicit MoveCommand(qreal x = qSNaN(), qreal y = qSNaN()):
        xCoordinate(x), yCoordinate(y)
    {}

    enum
    {
        Type = 3
    };

    qreal xCoordinate = qSNaN();
    qreal yCoordinate = qSNaN();

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class FlashCommand: public GerberCommand
{
public:
    enum
    {
        Type = 4
    };

    qreal xCoordinate = qSNaN();
    qreal yCoordinate = qSNaN();

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class SelectQuadrantCommand: public GerberCommand
{
public:
    explicit SelectQuadrantCommand(GerberQuadrant quadrant):
        quadrant(quadrant)
    {}

    enum
    {
        Type = 5
    };

    GerberQuadrant quadrant =InvalidQuadrant;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class BeginRegionCommand: public GerberCommand
{
public:
    enum
    {
        Type = 6
    };

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class EndRegionCommand: public GerberCommand
{
public:
    enum
    {
        Type = 7
    };

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class EndCommand: public GerberCommand
{
public:
    enum
    {
        Type = 8
    };

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class SelectUnitCommand: public GerberCommand
{
public:
    explicit SelectUnitCommand(GerberUnit unit):
        unit(unit)
    {}

    enum
    {
        Type = 9
    };

    GerberUnit unit = InvalidUnit;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class SelectPolarityCommand: public GerberCommand
{
public:
    enum
    {
        Type = 10
    };

    GerberPolarity polarity = InvalidPolarity;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class SetApertureAttributeCommand: public GerberCommand
{
public:
    enum
    {
        Type = 11
    };

    QString name;
    QStringList values;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class ClearApertureAttributeCommand: public GerberCommand
{
public:
    enum
    {
        Type = 12
    };

    QString name;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class SetFileAttributeCommand: public GerberCommand
{
public:
    enum
    {
        Type = 13
    };

    QString name;
    QStringList values;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class AddApertureCommand: public GerberCommand
{
public:
    enum
    {
        Type = 14
    };

    int apertureNumber = 0;
    QString templateName;
    QList<qreal> parameters;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class SelectApertureCommand: public GerberCommand
{
public:
    enum
    {
        Type = 15
    };
    int apertureNumber = 0;

    QString number;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class CommentCommand: public GerberCommand
{
public:
    explicit CommentCommand(const QString &code, const QString &text):
        code(code), text(text)
    {}

    enum
    {
        Type = 16
    };

    QString code;
    QString text;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

class SelectCoordinateNotationCommand: public GerberCommand
{
public:
    explicit SelectCoordinateNotationCommand(GerberCoordinateNotation notation):
        notation(notation)
    {}

    enum
    {
        Type = 17
    };

    GerberCoordinateNotation notation;

    // GerberCommand interface
public:
    virtual int type() const override { return Type; }
};

#endif // GERBERCOMMANDS_H

