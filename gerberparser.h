#ifndef GERBERPARSER_H
#define GERBERPARSER_H

#include <QObject>
#include <QMap>

#include "gerber.h"
#include "gerbercommands.h"

class GerberFileReaderContext
{
public:
    GerberCoordinateNotation coordinateNotation = InvalidCoordinateNotation;
    GerberZeroBehaviour zeroBehaviour = InvalidZeroBehaviour;
    int coordinateIntegralDigits = 2;
    int fractionalIntegralDigits = 5;

    QPointF readCoordinate(const QString &x, const QString &y) const;
    qreal readCoordinateNumber(const QString &str) const;
    void clear();
};

// TODO: trim leading and trailing spaces
// TODO: Add M02 to top level regexp (or implement stop?)
// See as well: https://github.com/curtacircuitos/pcb-tools/blob/master/gerber/rs274x.py
class GerberFileReader : public QObject
{
    Q_OBJECT
public:

    explicit GerberFileReader(QObject *parent = 0);
    ~GerberFileReader();

    void setData(const QByteArray &data);

    bool parse2();
    bool parse3(const QString &data);
    bool parse();
    bool isValid() const;
    QString errorString() const;

    QList<GerberCommand*> commands() const; // FIXME: takeCommands(), caller gain ownership, parser reset

    QMap<QString, int> codeStatistics() const;

signals:

public slots:

private:
    QByteArray m_data;
    QString m_errorString;
    QList<GerberCommand*> m_commands;
    GerberFileReaderContext m_context;
    int m_coordinateIntegralDigits = 0;
    int m_coordinateFractionalDigits = 0;
    QString m_coordinateFormat;

    QMap<QString, int> m_codeStats;
    int m_garbageChunks = 0;
    int m_garbageLength = 0;

    QString normaliseCode(const QString &code);
    qreal coordinateNumber(const QString &str);

    void handleCommentBlock(const QString &code, const QString &data);
    void handleFunctionBlock(const QString &code, const QString &data);
    void handleExtendedBlock(const QString &code, const QString &data);
    void handleGarbage(const QString &data);

    QMap<QString, GerberCommandReader *> m_readers;
    void addCommandReaders(QList<GerberCommandReader *> readers);
    void addCommandReader(GerberCommandReader *reader);

    int m_expectedStart;
};

#endif // GERBERPARSER_H
