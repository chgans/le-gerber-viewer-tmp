#ifndef GERBERPROCESSOR_H
#define GERBERPROCESSOR_H

#include <QObject>
#include <QPainterPath>

#include "gerber.h"
#include "gerberaperturetemplate.h"

class QGraphicsItem;

class GerberCommand;

// TODO: See p26&27 for graphics state parameters and operation codes
// See as well p59
// TODO When using D01/2 with G02 and G03, G74/5 should have been called
class GerberProcessor : public QObject
{
    Q_OBJECT
public:
    explicit GerberProcessor(QObject *parent = 0);

    bool processCommand(const GerberCommand *command);

    QList<QGraphicsItem *> objects() const;

private:
    GerberApertureTemplateDictionary m_apertureTemplateDictionary;
    GerberApertureDictionary m_apertureDictionary;
    GerberUnit m_unit = InvalidUnit;
    GerberInterpolation m_interpolation = InvalidInterpolation;
    GerberPolarity m_polarity = DarkPolarity;
    GerberQuadrant m_quadrant = InvalidQuadrant;
    GerberAperture *m_currentAperture = nullptr;
    QPointF m_currentPosition;

    QList<QGraphicsItem*> m_objects;

    // tmp
    bool m_inRegion = false;
    QPainterPath m_currentRegion;
    QList<QPainterPath> m_regions;
};

#endif // GERBERPROCESSOR_H
