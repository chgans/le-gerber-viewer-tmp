#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "gerber.h"
#include "gerbercommands.h"
#include "gerberparser.h"
#include "gerberprocessor.h"

#include <QListView>
#include <QFileSystemModel>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QDir>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(m_scene);
    ui->graphicsView->setInteractive(true);
    ui->graphicsView->setTransform(QTransform(1, 0, 0,
                                              0, -1, 0,
                                              0, 0, 1));
    ui->graphicsView->scale(3, 3);
    ui->graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
    ui->graphicsView->setRenderHints(QPainter::Antialiasing|QPainter::HighQualityAntialiasing);


    m_fileSystemModel = new QFileSystemModel(this);
    m_fileSystemModel->setRootPath(QDir::currentPath() + "/samples/");
    m_fileSystemModel->setFilter(QDir::AllEntries);
    ui->fileSystemListView->setModel(m_fileSystemModel);
    ui->fileSystemListView->setRootIndex(m_fileSystemModel->index(m_fileSystemModel->rootPath()));

    connect(ui->fileSystemListView, &QListView::clicked,
            this, &MainWindow::openFile);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFile()
{
    if (m_parser)
        delete m_parser;
    if (m_processor)
        delete m_processor;
    m_parser = new GerberFileReader;
    m_processor = new GerberProcessor;
    m_scene->clear();


    QString fileName = m_fileSystemModel->filePath(ui->fileSystemListView->currentIndex());
    if (fileName == "..")
    {
//        if (ui->fileSystemListView->rootIndex().parent().isValid())
//            ui->fileSystemListView->setRootIndex(ui->fileSystemListView->rootIndex().parent());
        QDir dir(m_fileSystemModel->rootPath());
        dir.cdUp();
        m_fileSystemModel->setRootPath(dir.absolutePath());
        ui->fileSystemListView->setRootIndex(m_fileSystemModel->index(m_fileSystemModel->rootPath()));
        return;
    }

    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
        qDebug() << "Error opening file" << file.errorString();
        return;
    }
    QByteArray data = file.readAll();

    m_parser->setData(data);
    if (!m_parser->parse2())
    {
        qDebug() << "Error parsing gerber data" << m_parser->errorString();
        return;
    }
    qDebug() << m_parser->codeStatistics();

    for (const GerberCommand *cmd: m_parser->commands())
    {
        bool result = m_processor->processCommand(cmd);
        if (!result)
        {
            qDebug() << "Command failed" << cmd->type();
        }
    }

    for (auto object: m_processor->objects())
    {
        //qDebug() << object;
        m_scene->addItem(object);
    }

    ui->graphicsView->fitInView(m_scene->itemsBoundingRect(), Qt::KeepAspectRatio);

}
