#include "gerbercommands.h"
#include "gerberparser.h"
#include <QDebug>

GerberCommandReader::GerberCommandReader(QChar family, const QString &code, const QString &format):
    m_family(family), m_code(code), m_format(QRegularExpression(format)),
    m_errorString(QString("Not implemented")),
    m_version(GerberVersionUnknown)
{
    m_format.optimize();
}

GerberCommandReader::~GerberCommandReader()
{

}

QChar GerberCommandReader::family() const
{
    return m_family;
}

QString GerberCommandReader::code() const
{
    return m_code;
}

QRegularExpression GerberCommandReader::format() const
{
    return m_format;
}

bool GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(match);
    Q_UNUSED(context);
    return false;
}

QString GerberCommandReader::errorString() const
{
    return m_errorString;
}

GerberVersion GerberCommandReader::version() const
{
    return m_version;
}

void GerberCommandReader::updateContext(GerberFileReaderContext *context) const
{
    Q_UNUSED(context);
}

QStringList GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *GerberCommandReader::deserialise() const
{
    return new NullGerberCommand();
}

void GerberCommandReader::clearErrorString()
{
    m_errorString.clear();
}

void GerberCommandReader::setVersion(GerberVersion version)
{
    m_version = version;
}

static QString StringRegExp("[a-zA-Z0-9_+\\-/!?<>”’(){}.\\|&@# ,;$:=]+");
static const QString LongCoordinateDataRegExp("(X(?<x>%1))?(Y(?<y>%1))?(I(?<i>%1))?(J(?<j>%1))?");
static const QString ShortCoordinateDataRegExp("(X(?<x>%1))?(Y(?<y>%1))?");

G01_GerberCommandReader::G01_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("01"), QStringLiteral("^(%1D0[12])?$").arg(LongCoordinateDataRegExp))
{

}

bool G01_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(context);
    if (match.capturedLength(1) != 0)
    {
        m_data = match.captured(1);
        setVersion(GerberVersion1);
    }
    else
        setVersion(GerberVersion2);
    clearErrorString();
    return true;
}

QStringList G01_GerberCommandReader::decompose() const
{
    if (m_data.isEmpty())
        return QStringList();
    else
        return QStringList() << QString("%1*").arg(m_data);
}

GerberCommand *G01_GerberCommandReader::deserialise() const
{
    auto cmd = new SelectInterpolationCommand;
    cmd->interpolation = LinearInterpolation;
    return cmd;
}

G02_GerberCommandReader::G02_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("02"), QStringLiteral("^(%1D0[12])?$").arg(LongCoordinateDataRegExp))
{

}

bool G02_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(context);
    if (match.capturedLength(1) != 0)
    {
        m_data = match.captured(1);
        setVersion(GerberVersion1);
    }
    else
        setVersion(GerberVersion2);
    clearErrorString();
    return true;
}

QStringList G02_GerberCommandReader::decompose() const
{
    if (m_data.isEmpty())
        return QStringList();
    else
        return QStringList() << QString("%1*").arg(m_data);
}

GerberCommand *G02_GerberCommandReader::deserialise() const
{
    auto cmd = new SelectInterpolationCommand;
    cmd->interpolation = ClockWiseInterpolation;
    return cmd;
}

G03_GerberCommandReader::G03_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("03"), QStringLiteral("^(%1D0[12])?$").arg(LongCoordinateDataRegExp))
{

}

bool G03_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(context);
    if (match.capturedLength(1) != 0)
    {
        m_data = match.captured(1);
        setVersion(GerberVersion1);
    }
    else
        setVersion(GerberVersion2);
    clearErrorString();
    return true;
}

QStringList G03_GerberCommandReader::decompose() const
{
    if (m_data.isEmpty())
        return QStringList();
    else
        return QStringList() << QString("%1*").arg(m_data);
}

GerberCommand *G03_GerberCommandReader::deserialise() const
{
    auto cmd = new SelectInterpolationCommand;
    cmd->interpolation = CounterClockWiseInterpolation;
    return cmd;
}


// TODO: custom vs standard comment, see p. 95
G04_GerberCommandReader::G04_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("04"), QStringLiteral("^%1$").arg(StringRegExp))
{

}

bool G04_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(context);
    m_text = match.captured(0);
    clearErrorString();
    setVersion(GerberVersion3);
    return true;
}

QStringList G04_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *G04_GerberCommandReader::deserialise() const
{
    return new CommentCommand("G04", m_text);
}

G36_GerberCommandReader::G36_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("36"), QStringLiteral("^$"))
{

}

bool G36_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(context);
    Q_UNUSED(match);
    clearErrorString();
    setVersion(GerberVersion1);
    return true;
}

QStringList G36_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *G36_GerberCommandReader::deserialise() const
{
    return new BeginRegionCommand();
}

G37_GerberCommandReader::G37_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("37"), QStringLiteral("^$"))
{

}

bool G37_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(match);
    Q_UNUSED(context);
    clearErrorString();
    setVersion(GerberVersion1);
    return true;
}

QStringList G37_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *G37_GerberCommandReader::deserialise() const
{
    return new EndRegionCommand();
}

G54_GerberCommandReader::G54_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("54"), QStringLiteral("^(D[1-9][0-9]+)?$"))
{

}

bool G54_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(context);
    m_data = match.captured(0);
    clearErrorString();
    setVersion(GerberVersion1);
    return true;
}

QStringList G54_GerberCommandReader::decompose() const
{
    if (m_data.isEmpty())
        return QStringList();
    else
        return QStringList() << QString("%1*").arg(m_data);
}

GerberCommand *G54_GerberCommandReader::deserialise() const
{
    return new NullGerberCommand();
}

G55_GerberCommandReader::G55_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("55"), QStringLiteral("^(%1D03)?$").arg(ShortCoordinateDataRegExp))
{

}

bool G55_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(context);
    m_data = match.captured(0);
    clearErrorString();
    if (m_data.count() != 0)
        setVersion(GerberVersion1);
    else
        setVersion(GerberVersion2);
    return true;
}

QStringList G55_GerberCommandReader::decompose() const
{
    if (m_data.isEmpty())
        return QStringList();
    else
        return QStringList() << QString("%1*").arg(m_data);
}

GerberCommand *G55_GerberCommandReader::deserialise() const
{
    return new NullGerberCommand();
}

G70_GerberCommandReader::G70_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("70"), QStringLiteral("^$"))
{

}

bool G70_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(context);
    Q_UNUSED(match);
    clearErrorString();
    setVersion(GerberVersion1);
    return true;
}

QStringList G70_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *G70_GerberCommandReader::deserialise() const
{
    return new SelectUnitCommand(ImperialUnit);
}

G71_GerberCommandReader::G71_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("71"), QStringLiteral("^$"))
{

}

bool G71_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(context);
    Q_UNUSED(match);
    clearErrorString();
    setVersion(GerberVersion1);
    return true;
}

QStringList G71_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *G71_GerberCommandReader::deserialise() const
{
    return new SelectUnitCommand(MetricUnit);
}

G74_GerberCommandReader::G74_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("74"), QStringLiteral("^$"))
{

}

bool G74_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(match);
    Q_UNUSED(context);
    clearErrorString();
    setVersion(GerberVersion1);
    return true;
}

QStringList G74_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *G74_GerberCommandReader::deserialise() const
{
    return new SelectQuadrantCommand(SingleQuadrant);
}

G75_GerberCommandReader::G75_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("75"), QStringLiteral("^$"))
{

}

bool G75_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(match);
    Q_UNUSED(context);
    clearErrorString();
    setVersion(GerberVersion1);
    return true;
}

QStringList G75_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *G75_GerberCommandReader::deserialise() const
{
    return new SelectQuadrantCommand(MultipleQuadrant);
}

G90_GerberCommandReader::G90_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("90"), QStringLiteral("^$"))
{
}

bool G90_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(match);
    Q_UNUSED(context);
    clearErrorString();
    setVersion(GerberVersion1);
    return true;
}

QStringList G90_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *G90_GerberCommandReader::deserialise() const
{
    return new SelectCoordinateNotationCommand(AbsoluteCoordinateNotation);
}

G91_GerberCommandReader::G91_GerberCommandReader():
    GerberCommandReader (QChar('G'), QStringLiteral("91"), QStringLiteral("^$"))
{

}

bool G91_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(match);
    Q_UNUSED(context);
    clearErrorString();
    setVersion(GerberVersion1);
    return true;
}

QStringList G91_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *G91_GerberCommandReader::deserialise() const
{
    return new SelectCoordinateNotationCommand(IncrementalCoordinateNotation);
}

/*
 * TBD:
 *  - validate and convert numbers according to format
 *  - decompose X, Y, I, J?
 *  - pass a context object? eg w/ coordinate reader?
 *  - let the executor/provcessor deal with this?
 */

D01_GerberCommandReader::D01_GerberCommandReader():
    GerberCommandReader (QChar('D'), QStringLiteral("01"),
                         QStringLiteral("^(X(?<x>%1))?(Y(?<y>%1))?(I(?<i>%1))?(J(?<j>%1))?$")
                         .arg(QStringLiteral("[+\\-]?[0-9]{1,10}")))
{

}

bool D01_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    m_xy = context->readCoordinate(match.captured("x"), match.captured("y"));
    m_ij = context->readCoordinate(match.captured("i"), match.captured("j"));
    return true;
}

QStringList D01_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *D01_GerberCommandReader::deserialise() const
{
    auto cmd = new InterpolateCommand;
    cmd->xCoordinate = m_xy.x();
    cmd->yCoordinate = m_xy.y();
    cmd->xDistance = m_ij.x();
    cmd->yDistance = m_ij.y();
    return cmd;
}

D02_GerberCommandReader::D02_GerberCommandReader():
    GerberCommandReader (QChar('D'), QStringLiteral("02"),
                         QStringLiteral("^(X(?<x>%1))?(Y(?<y>%1))?$")
                         .arg(QStringLiteral("[+\\-]?[0-9]{1,10}")))
{

}

bool D02_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    m_xy = context->readCoordinate(match.captured("x"), match.captured("y")); // FIXME: check for errors
    return true;
}

QStringList D02_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *D02_GerberCommandReader::deserialise() const
{
    auto cmd = new MoveCommand;
    cmd->xCoordinate = m_xy.x();
    cmd->yCoordinate = m_xy.y();
    return cmd;
}

D03_GerberCommandReader::D03_GerberCommandReader():
    GerberCommandReader (QChar('D'), QStringLiteral("03"),
                         QStringLiteral("^(X(?<x>%1))?(Y(?<y>%1))?$")
                         .arg(QStringLiteral("[+\\-]?[0-9]{1,10}")))
{

}

bool D03_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    m_xy = context->readCoordinate(match.captured("x"), match.captured("y")); // FIXME: check for errors
    return true;
}

QStringList D03_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *D03_GerberCommandReader::deserialise() const
{
    auto cmd = new FlashCommand;
    cmd->xCoordinate = m_xy.x();
    cmd->yCoordinate = m_xy.y();
    return cmd;
}

DNN_GerberCommandReader::DNN_GerberCommandReader():
    GerberCommandReader (QChar('D'), QStringLiteral("04"), // FIXME
                         QStringLiteral("^[1-9][0-9]+$"))
{

}

bool DNN_GerberCommandReader::parse(const QRegularExpressionMatch &match, const GerberFileReaderContext *context)
{
    Q_UNUSED(context);
    m_number = match.captured(0);
    return true;
}

QStringList DNN_GerberCommandReader::decompose() const
{
    return QStringList();
}

GerberCommand *DNN_GerberCommandReader::deserialise() const
{
    auto cmd = new SelectApertureCommand;
    cmd->number = m_number;
    return cmd;
}
