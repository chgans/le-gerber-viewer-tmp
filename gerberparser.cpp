#include "gerberparser.h"

#include <QDebug>
#include <QRegularExpression>


namespace GerberParserPrivate
{
static QString NUMBER("[\\+-]?\\d+");
static QString DECIMAL("[\\+-]?\\d+([.]?\\d+)?");
static QString COORDFORMAT("(?<integral>[0-6])(?<fractional>[4-6])");

static QString STRING("[a-zA-Z0-9_+\\-/!?<>”’(){}.\\|&@# ,;$:=]+");
static QString NAME("[a-zA-Z_\\.$][a-zA-Z_\\.$0-9]*");

static QString FIELD("[a-zA-Z0-9_+\\-/!?<>”’(){}.\\|&@# ;$:=]+"); // STRING w/o ','

static QString APERTURENUMBER("[1-9][0-9]+");

static QString EXTENDED("%(?<ecode>[A-Z][A-Z])(?<edata>[^%*]*\\*)%"); // FIXME: [^%]
static QString FUNCTION("(?<fdata>[^*%]*)(?<fcode>[A-Z][0-9]+)\\*");
static QString COMMENT("(?<ccode>G0?4) ?(?<cdata>[^*%]*)\\*");

static QString STREAM = QString("(?<comment>%1)|(?<function>%2)|(?<extended>%3)")
        .arg(COMMENT).arg(FUNCTION).arg(EXTENDED);
}

GerberFileReader::GerberFileReader(QObject *parent) : QObject(parent)
{

    QList<GerberCommandReader *> readers;
    readers << new G01_GerberCommandReader()
            << new G02_GerberCommandReader()
            << new G03_GerberCommandReader()
            << new G04_GerberCommandReader()
            << new G36_GerberCommandReader()
            << new G37_GerberCommandReader()
            << new G54_GerberCommandReader()
            << new G55_GerberCommandReader()
            << new G70_GerberCommandReader()
            << new G71_GerberCommandReader()
            << new G74_GerberCommandReader()
            << new G75_GerberCommandReader()
            << new G90_GerberCommandReader()
            << new G91_GerberCommandReader()
            << new D01_GerberCommandReader()
            << new D02_GerberCommandReader()
            << new D03_GerberCommandReader()
            << new DNN_GerberCommandReader();
    addCommandReaders(readers);
}

GerberFileReader::~GerberFileReader()
{
    qDeleteAll(m_commands);
}

void GerberFileReader::setData(const QByteArray &data)
{
    m_data = data;
    m_data.replace('\n', QByteArray()).replace('\r', QByteArray()).replace('\t', QByteArray());
    m_data = m_data.trimmed();
    m_errorString.clear();
    qDeleteAll(m_commands);
    m_commands.clear();
}

bool GerberFileReader::parse2()
{
    m_expectedStart = 0;
    m_context.clear();
    return parse3(m_data);
}


bool GerberFileReader::parse3(const QString &data)
{
    QString PreRegExp = QString("(?<precode>[GDM][0-9]+)(?<predata>[^*%]*)\\*");
    QString PostRegExp = QString("(?<postdata>[^*%]*)(?<postcode>[GDM][0-9]+?)\\*");
    QString ExtendedRegExp = QString("%(?<xcode>[A-Za-z]{2})(?<xdata>[^%]*\\*)%");
    QString RegExp = QString("(?<pre>%1)|(?<post>%2)|(?<extended>%3)").arg(PreRegExp).arg(PostRegExp).arg(ExtendedRegExp);
    static const QRegularExpression re(RegExp);
    if (!re.isValid())
    {
        m_errorString = QStringLiteral("Internal error");
        return false;
    }
    re.optimize();

    QRegularExpressionMatchIterator matches = re.globalMatch(data);

    int expectedStart = 0;
    while (matches.hasNext())
    {
        QRegularExpressionMatch match = matches.next();

        int start = match.capturedStart(0);
        if (start != m_expectedStart)
        {
            handleGarbage(data.mid(expectedStart, start-m_expectedStart));
        }

        QString code;
        QString data;
        if (match.captured("pre").length() != 0)
        {
            code = match.captured("precode");
            data = match.captured("predata");
        }
        else if (match.capturedLength("post") != 0)
        {
            code = match.captured("postcode");
            data = match.captured("postdata");
        }
        else if (match.capturedLength("extended") != 0)
        {
            code = match.captured("xcode");
            data = match.captured("xdata");
        }
        else
        {
            // Can happen w/ simple and no precode and no postcode
            qDebug() << "Internal error:" << match.capturedTexts();
            return false;
        }

        if (m_readers.contains(code))
        {
            GerberCommandReader *reader = m_readers[code];
            QRegularExpression re = reader->format();
            QRegularExpressionMatch match = re.match(data);
            if (reader->parse(match, &m_context))
            {
                reader->updateContext(&m_context);
                auto cmd = reader->deserialise();
                m_commands.append(cmd);
                for (auto command: reader->decompose()) // Breaks the ignored tracking ... :(
                {
                    parse3(command);
                }
            }
            else
            {
                qDebug() << "Cannot parse command" << code << data;
            }
        }
        else
        {
            qDebug() << "Unhandled command" << code << data;
        }
        m_expectedStart = match.capturedEnd(0);
    }
    if (m_expectedStart != (data.count()))
    {
        handleGarbage(data.mid(expectedStart, data.count()-m_expectedStart));
    }

    return true;
}

void GerberFileReader::handleGarbage(const QString &data)
{
    m_garbageChunks++;
    m_garbageLength += data.count();
    //qDebug() << "Warning: ignoring garbage data:" << data;
}

void GerberFileReader::addCommandReaders(QList<GerberCommandReader *> readers)
{
    for (auto reader: readers)
        addCommandReader(reader);
}

void GerberFileReader::addCommandReader(GerberCommandReader *reader)
{
    QString prefix = QStringLiteral("%1%2").arg(reader->family()).arg(reader->code());
    m_readers.insert(prefix, reader);
}

bool GerberFileReader::parse()
{
    QRegularExpression re(GerberParserPrivate::STREAM); // TODO: "\\s*(STREAM)*\\s*"
    if (!re.isValid())
    {
        m_errorString = QStringLiteral("Internal error");
        return false;
    }
    if (m_data.isNull() || m_data.isEmpty())
    {
        m_errorString = QStringLiteral("No input data");
        return false;
    }

    const QString text = QString::fromLocal8Bit(m_data);
    QRegularExpressionMatchIterator matches = re.globalMatch(text);

    int expectedStart = 0;
    while (matches.hasNext())
    {
        QRegularExpressionMatch match = matches.next();

        int start = match.capturedStart(0);
        if (start != expectedStart)
        {
            handleGarbage(text.mid(expectedStart, start-expectedStart));
        }
        expectedStart = match.capturedEnd(0);

        if (match.capturedLength("comment") != 0)
        {
            m_codeStats[match.captured("ccode")]++;
            handleCommentBlock(match.captured("ccode"), match.captured("cdata"));
        }
        else if (match.capturedLength("function") != 0)
        {
            m_codeStats[match.captured("fcode")]++;
            handleFunctionBlock(match.captured("fcode"), match.captured("fdata"));
        }
        else if (match.capturedLength("extended") != 0)
        {
            m_codeStats[match.captured("ecode")]++;
            handleExtendedBlock(match.captured("ecode"), match.captured("edata"));
        }
        else
        {
            // Cannot happen since the regexp matches all the usecases above
            qDebug() << "Internal error:" << match.capturedTexts();
            return false;
        }
    }
    if (expectedStart != (text.count()))
    {
        handleGarbage(text.mid(expectedStart, text.count()-expectedStart));
    }

    return true;
}

bool GerberFileReader::isValid() const
{
    return !m_data.isNull() && m_errorString.isEmpty();
}

QString GerberFileReader::errorString() const
{
    return m_errorString;
}

QList<GerberCommand *> GerberFileReader::commands() const
{
    return m_commands;
}

QMap<QString, int> GerberFileReader::codeStatistics() const
{
    return m_codeStats;
}

qreal GerberFileReader::coordinateNumber(const QString &str)
{
    static const int base = 10;
    static const QChar fill('0');
    int maxLength = m_coordinateIntegralDigits + m_coordinateFractionalDigits;
    QString number = QString("%1").arg(str.toInt(), maxLength, base, fill);
    number.insert(m_coordinateIntegralDigits, QChar('.'));
    return number.toDouble();
}

void GerberFileReader::handleCommentBlock(const QString &code, const QString &data)
{
    auto cmd = new CommentCommand(code, data);
    m_commands.append(cmd);
}

void GerberFileReader::handleFunctionBlock(const QString &code, const QString &data)
{
    if (code == "G01")
    {
        auto cmd = new SelectInterpolationCommand;
        cmd->interpolation = LinearInterpolation;
        m_commands.append(cmd);
    }
    else if (code == "G02")
    {
        auto cmd = new SelectInterpolationCommand;
        cmd->interpolation = ClockWiseInterpolation;
        m_commands.append(cmd);
    }
    else if (code == "G03")
    {
        auto cmd = new SelectInterpolationCommand;
        cmd->interpolation = CounterClockWiseInterpolation;
        m_commands.append(cmd);

    }
    else if (code == "G36")
    {
        auto cmd = new BeginRegionCommand;
        m_commands.append(cmd);
    }
    else if (code == "G37")
    {
        auto cmd = new EndRegionCommand;
        m_commands.append(cmd);
    }
    else if (code == "G74")
    {
        auto cmd = new SelectQuadrantCommand(SingleQuadrant);
        m_commands.append(cmd);
    }
    else if (code == "G75")
    {
        auto cmd = new SelectQuadrantCommand(MultipleQuadrant);
        m_commands.append(cmd);
    }
    else if (code == "D01")
    {
        static const QRegularExpression re(QString("^(?<gcode>G0?[123])?(X(?<x>%1))?(Y(?<y>%1))?(I(?<i>%1))?(J(?<j>%1))?$")
                                           .arg(m_coordinateFormat));
        auto cmd = new InterpolateCommand;
        auto match = re.match(data);
        if (match.capturedLength("gcode"))
            handleFunctionBlock(match.captured("gcode"), QByteArray());
        if (match.capturedLength("x"))
            cmd->xCoordinate = coordinateNumber(match.captured("x"));
        if (match.capturedLength("y"))
            cmd->yCoordinate = coordinateNumber(match.captured("y"));
        if (match.capturedLength("i"))
            cmd->xDistance = coordinateNumber(match.captured("i"));
        if (match.capturedLength("j"))
            cmd->yDistance = coordinateNumber(match.captured("j"));
        m_commands.append(cmd);
    }
    else if (code == "D02")
    {
        // Same as w/ D01: (?<gcode>G0?[123])?(X(?<x>%1))?(Y(?<y>%1))?$
        static const QRegularExpression re(QString("^(X(?<x>%1))?(Y(?<y>%1))?$")
                                           .arg(m_coordinateFormat));
        auto cmd = new MoveCommand;
        auto match = re.match(data);
        if (match.capturedLength("x"))
            cmd->xCoordinate = coordinateNumber(match.captured("x"));
        if (match.capturedLength("y"))
            cmd->yCoordinate = coordinateNumber(match.captured("y"));
        m_commands.append(cmd);
    }
    else if (code == "D03")
    {
        // FIXME: Optional G55 prefix
        static const QRegularExpression re(QString("^(X(?<x>%1))?(Y(?<y>%1))?$")
                                           .arg(m_coordinateFormat));
        auto cmd = new FlashCommand;
        auto match = re.match(data);
        if (match.capturedLength("x"))
            cmd->xCoordinate = coordinateNumber(match.captured("x"));
        if (match.capturedLength("y"))
            cmd->yCoordinate = coordinateNumber(match.captured("y"));
        m_commands.append(cmd);
    }
    else if (code == "M02")
    {
        // M00 has same effect
        auto cmd = new EndCommand;
        m_commands.append(cmd);
    }
    else if (code == "G54" || code.at(0) == QChar('D'))
    {
        static const QRegularExpression re(QString("^D(?<number>%1)$").arg(GerberParserPrivate::APERTURENUMBER));
        QString input = code == "G54" ? data : code;
        auto match = re.match(input);
        if (!match.hasMatch())
        {
            qDebug() << "Invalid Dnnn command" << code;
            return;
        }
        auto cmd = new SelectApertureCommand;
        cmd->apertureNumber = match.captured("number").toInt();
        m_commands.append(cmd);
    }
    // G55D03, G70, G71, G90, G91
    // M00, M01
    else
    {
        qDebug() << "Unknown Function Command" << code << data;
    }
}

// TODO: command specification:
//  - code
//  - permissiveDataRegExp
//  - Gerber level: D, X, X2 (command as a block, not injected command)
//  - isDeprecated, isDangerous, isUnsupported (dynamic, command as a block, not injected command)
//  - validate(data), process(data), allow for extracted command injection/split
// TODO: 1st pass: parse permissively (grammar: code+regexp) (inc sub command injection here?)
//                 TBD: auto-detect gerber level?
// TODO: 2nd pass: process (inc sub command injection here?)
// TODO: 3rd pass: high level validation (enforce sequence, etc..) + hotfix => create a rule class?
// TODO: 4th pass: execute commands on processor => graphics objects w/ polarity property?
// TODO: 5th pass: render graphics object (inc. polarity)
//
// IDEA: Collect comments, scan for generator signatures
//
// Deprecated Coordinates data w/o Operation code (p. 172)
// Deprecated rectangular hole in standard apertures (p. 173)
//
// Deprecated extended codes ():
// AS(?<>(AXBY|AYBX))
// FS(?<>(L|T))(?<>(A|I))
// IN(?<>%1) % STRING
// IP(?<>(POS|NEG))
// IR(?<>(0|90|180|270)
// LN(?<>%1) % STRING
// MI(A(?<>(0|1)))?(B(?<>(0|1)))?
// OF(A(?<>%1))?(B(?<>%1))? % DECIMAL w/ 0<=n<=99999.99999
// SF(A(?<>%1))?(B(?<>%1))? % DECIMAL w/ 0.0001<=n<=999.99999
void GerberFileReader::handleExtendedBlock(const QString &code, const QString &data)
{
    if (code == "FS")
    {
        //static const QRegularExpression re(QString("^LAX(?<ix>[0-5])(?<fx>[4-6])Y(?<iy>[0-5])(?<fy>[4-6])\\*$"));
        static const QRegularExpression re(QString("^LAX(?<ix>[0-5])(?<fx>[0-6])Y(?<iy>[0-5])(?<fy>[0-6])\\*$"));
        auto match = re.match(data);
        if (!match.hasMatch())
        {
            qDebug() << "Invalid FS command (could not parse)" << code << data;
            return;
        }
        if (match.captured("ix") != match.captured("iy") ||
                match.captured("fx") != match.captured("fy"))
        {
            qDebug() << "Invalid FS command (X/Y format mismatch)" << code << data;
            return;
        }
        m_coordinateIntegralDigits = match.captured("ix").toInt();
        m_coordinateFractionalDigits = match.captured("fx").toInt();
        m_coordinateFormat = QString("[+\\-]?[0-9]{%1,%2}").arg(0).arg(m_coordinateIntegralDigits + m_coordinateFractionalDigits);
    }
    else if (code == "MO")
    {
        static const QRegularExpression re(QString("^(IN|MM)\\*$"));
        auto match = re.match(data);
        if (!match.hasMatch())
        {
            qDebug() << "Invalid MO command" << code << data;
            return;
        }
        SelectUnitCommand *cmd;
        if (match.captured(0) == "IN")
            cmd = new SelectUnitCommand(ImperialUnit);
        else
            cmd = new SelectUnitCommand(MetricUnit);
        m_commands.append(cmd);
    }
    else if (code == "AD")
    {
        static const QRegularExpression re(QString("^D(?<number>%1)(?<name>%2)(,(?<modifiers>%3(X%3)*))?\\*$")
                                           .arg(GerberParserPrivate::APERTURENUMBER)
                                           .arg(GerberParserPrivate::NAME)
                                           .arg(GerberParserPrivate::DECIMAL));
        auto match = re.match(data);
        if (!match.hasMatch())
        {
            qDebug() << "Invalid AD command" << code << data << re.isValid() << re.errorString();
            return;
        }
        auto cmd = new AddApertureCommand;
        cmd->apertureNumber = match.captured("number").toInt();
        cmd->templateName = match.captured("name");
        for (const QString &modifier: match.captured("modifiers").split(QChar('X')))
        {
            cmd->parameters.append(modifier.toDouble());
        }
        m_commands.append(cmd);
    }
    else if (code == "AM")
    {
        // TODO: Macro aperture
    }
    else if (code == "LP")
    {
        static const QRegularExpression re(QString("^(C|D)\\*$"));
        auto match = re.match(data);
        if (!match.hasMatch())
        {
            qDebug() << "Invalid LP command" << code << data;
            return;
        }
        auto cmd = new SelectPolarityCommand;
        if (match.captured(0) == "C")
            cmd->polarity = ClearPolarity;
        else
            cmd->polarity = DarkPolarity;
        m_commands.append(cmd);
    }
    else if (code == "SR")
    {
        // TODO: Step and repeat
    }
    else if (code == "TF" || code == "TA")
    {
        static const QRegularExpression re(QString("^(?<name>%1)(?<value>(,%2)*)\\*$")
                                           .arg(GerberParserPrivate::NAME)
                                           .arg(GerberParserPrivate::FIELD));
        auto match = re.match(data);
        if (!match.hasMatch())
        {
            qDebug() << "Invalid TF command" << code << data;
            return;
        }
        auto name = match.captured("name");
        auto values = match.captured("value").split(QChar(','));
        if (!values.isEmpty())
            values.removeFirst(); // First always empty, since regexp starts with ','

        if (code == "TF")
        {
            auto cmd = new SetFileAttributeCommand;
            cmd->name = name;
            cmd->values = values;
            m_commands.append(cmd);
        }
        else
        {
            auto cmd = new SetApertureAttributeCommand;
            cmd->name = name;
            cmd->values = values;
            m_commands.append(cmd);
        }
    }
    else if (code == "TD")
    {
        static const QRegularExpression re(QString("^(?<name>%1)\\*$")
                                           .arg(GerberParserPrivate::NAME));
        auto match = re.match(data);
        if (!match.hasMatch())
        {
            qDebug() << "Invalid TD command" << code << data;
            return;
        }
        auto cmd = new ClearApertureAttributeCommand;
        cmd->name = match.captured("name");
        m_commands.append(cmd);
    }
    else
    {
        qDebug() << "Unknown Extended Command" << code << data;
    }
}

QPointF GerberFileReaderContext::readCoordinate(const QString &x, const QString &y) const
{
    return QPointF(readCoordinateNumber(x), readCoordinateNumber(y));
}

qreal GerberFileReaderContext::readCoordinateNumber(const QString &str) const
{
    if (str.isEmpty())
        return qSNaN();

    int length = coordinateIntegralDigits + fractionalIntegralDigits;
    int paddingCount = length - str.length();
    if (paddingCount < 0 )
    {
        paddingCount = 0; // Should not happen, emit warning
    }

    QString padding = QString(paddingCount, QChar('0'));
    QString number = str;
    if (zeroBehaviour == TrailingZeroOmmited)
    {
        number = number + padding;
    }
    else // Default
    {
        number = padding + number;
    }

    number.insert(coordinateIntegralDigits, QChar('.'));
    return number.toDouble();
}

void GerberFileReaderContext::clear()
{
    coordinateNotation = InvalidCoordinateNotation;
    zeroBehaviour = InvalidZeroBehaviour;
    coordinateIntegralDigits = 2;
    fractionalIntegralDigits = 5;
}
