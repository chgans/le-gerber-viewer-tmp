#ifndef GERBERAPERTURETEMPLATE_H
#define GERBERAPERTURETEMPLATE_H

#include <QString>
#include <QList>

class GerberAperture;

class GerberApertureTemplate
{
public:
    GerberApertureTemplate(const QString &name, const QString &friendlyName = QString(),
                           bool isBuiltIn = false);
    virtual ~GerberApertureTemplate();

    QString name;
    QString friendlyName;
    bool isBuiltIn;

    virtual GerberAperture *instanciate(const QList<qreal> parameters) const = 0;
};

class GerberCircleApertureTemplate: public GerberApertureTemplate
{
public:
    GerberCircleApertureTemplate();

    // GerberApertureTemplate interface
public:
    virtual GerberAperture *instanciate(const QList<qreal> parameters) const override;
};

class GerberRectApertureTemplate: public GerberApertureTemplate
{
public:
    GerberRectApertureTemplate();

    // GerberApertureTemplate interface
public:
    virtual GerberAperture *instanciate(const QList<qreal> parameters) const override;
};

class GerberObroundApertureTemplate: public GerberApertureTemplate
{
public:
    GerberObroundApertureTemplate();

    // GerberApertureTemplate interface
public:
    virtual GerberAperture *instanciate(const QList<qreal> parameters) const override;
};

class GerberPolygonApertureTemplate: public GerberApertureTemplate
{
public:
    GerberPolygonApertureTemplate();

    // GerberApertureTemplate interface
public:
    virtual GerberAperture *instanciate(const QList<qreal> parameters) const override;
};

#include <QPainterPath>

class GerberAperture
{
public:
    int apertureNumber;
    QPainterPath shape;
    qreal width = 0.0; // FIXME
};

#include <QMap>

typedef QMap<QString, GerberApertureTemplate*> GerberApertureTemplateDictionary;
typedef QMap<int, GerberAperture*> GerberApertureDictionary;

#endif // GERBERAPERTURETEMPLATE_H
