#ifndef GERBER_H
#define GERBER_H

enum GerberVersion
{
    GerberVersionUnknown = 0,
    GerberVersion1,
    GerberVersion2, // X
    GerberVersion3 // X3
};

enum GerberInterpolation
{
    InvalidInterpolation = 0,
    LinearInterpolation,
    ClockWiseInterpolation,
    CounterClockWiseInterpolation
};

enum GerberQuadrant
{
    InvalidQuadrant = 0,
    SingleQuadrant,
    MultipleQuadrant,
};

enum GerberUnit
{
    InvalidUnit = 0,
    MetricUnit,
    ImperialUnit
};

enum GerberPolarity
{
    InvalidPolarity = 0,
    ClearPolarity,
    DarkPolarity
};

enum GerberCoordinateNotation
{
    InvalidCoordinateNotation = 0,
    AbsoluteCoordinateNotation,
    IncrementalCoordinateNotation
};

enum GerberZeroBehaviour
{
    InvalidZeroBehaviour = 0,
    LeadingZeroOmmited,
    TrailingZeroOmmited,
};

#endif // GERBER_H
