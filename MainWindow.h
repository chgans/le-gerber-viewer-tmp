#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QGraphicsScene;
class GerberFileReader;
class GerberProcessor;
class QFileSystemModel;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void openFile();

private:
    Ui::MainWindow *ui;
    QFileSystemModel *m_fileSystemModel;
    GerberFileReader *m_parser = nullptr;
    GerberProcessor *m_processor = nullptr;
    QGraphicsScene *m_scene = nullptr;
};

#endif // MAINWINDOW_H
