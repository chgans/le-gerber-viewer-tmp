QT += core gui widgets

CONFIG += c++11

TARGET = gerber-reader
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    gerberparser.cpp \
    gerberaperturetemplate.cpp \
    gerberprocessor.cpp \
    MainWindow.cpp \
    gerbercommands.cpp

HEADERS += \
    gerberparser.h \
    gerberaperturetemplate.h \
    gerbercommands.h \
    gerber.h \
    gerberprocessor.h \
    MainWindow.h

DISTFILES += \
    figure-20.gbr \
    figure-18.gbr \
    figure-17.gbr \
    figure-19.gbr \
    figure-21.gbr \
    figure-25.gbr \
    figure-6.gbr \
    figure-7.gbr \
    figure-8.gbr \
    figure-13.gbr

FORMS += \
    MainWindow.ui
