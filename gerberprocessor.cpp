#include "gerberprocessor.h"
#include "gerbercommands.h"

#include <QDebug>
#include <QGraphicsPathItem>
#include <QBrush>
#include <QPen>
#include <QtMath>

GerberProcessor::GerberProcessor(QObject *parent) : QObject(parent)
{
    GerberApertureTemplate *apertureTemplate = new GerberCircleApertureTemplate;
    m_apertureTemplateDictionary.insert(apertureTemplate->name, apertureTemplate);
    apertureTemplate = new GerberRectApertureTemplate;
    m_apertureTemplateDictionary.insert(apertureTemplate->name, apertureTemplate);
    apertureTemplate = new GerberObroundApertureTemplate;
    m_apertureTemplateDictionary.insert(apertureTemplate->name, apertureTemplate);
    apertureTemplate = new GerberPolygonApertureTemplate;
    m_apertureTemplateDictionary.insert(apertureTemplate->name, apertureTemplate);
}

bool GerberProcessor::processCommand(const GerberCommand *command)
{
    switch (command->type())
    {
        case CommentCommand::Type:
        {
            auto *cmd = static_cast<const CommentCommand*>(command);
            return true;
        }
        case SelectUnitCommand::Type:
        {
            auto *cmd = static_cast<const SelectUnitCommand*>(command);
            if (m_unit != InvalidUnit)
            {
                qDebug() << "Cannot change unit twice";
                return false;
            }
            m_unit = cmd->unit;
            return true;
        }
        case SelectInterpolationCommand::Type:
        {
            //qDebug() << "Select interpolation";
            auto *cmd = static_cast<const SelectInterpolationCommand*>(command);
            m_interpolation = cmd->interpolation;
            return true;
        }
        case InterpolateCommand::Type: // D01
        {
            auto *cmd = static_cast<const InterpolateCommand*>(command);

            switch (m_interpolation)
            {
                case InvalidInterpolation:
                {
                    qDebug() << "Cannot interpolate";
                    return false;
                }
                case LinearInterpolation:
                {
                    QPointF endPoint(cmd->xCoordinate, cmd->yCoordinate);
                    if (qIsNaN(endPoint.x()))
                        endPoint.setX(m_currentPosition.x());
                    if (qIsNaN(endPoint.y()))
                        endPoint.setY(m_currentPosition.y());

                    if (!m_inRegion)
                    {
                        // FIXME: only C and R aperture allowed, only C supported
                        auto object = new QGraphicsLineItem;
                        object->setLine(QLineF(m_currentPosition, endPoint));
                        object->setPen(QPen(Qt::darkRed, m_currentAperture->width, Qt::SolidLine,
                                            Qt::RoundCap, Qt::RoundJoin));
                        m_objects.append(object);
                    }
                    else
                    {
                        if (m_currentRegion.isEmpty())
                            m_currentRegion.moveTo(m_currentPosition);
                        m_currentRegion.lineTo(endPoint);
                    }

                    m_currentPosition = endPoint;

                    return true;
                }
                case ClockWiseInterpolation:
                case CounterClockWiseInterpolation:
                {
                    qDebug() << cmd->xCoordinate << cmd->yCoordinate << cmd->xDistance << cmd->yDistance;
                    qreal x = qIsNaN(cmd->xCoordinate) ? m_currentPosition.x() : cmd->xCoordinate;
                    qreal y = qIsNaN(cmd->yCoordinate) ? m_currentPosition.y() : cmd->yCoordinate;
                    qreal i = qIsNaN(cmd->xDistance) ? 0 : cmd->xDistance;
                    qreal j = qIsNaN(cmd->yDistance) ? 0 : cmd->yDistance;

                    // Fig 13.
                    if (m_quadrant == SingleQuadrant)
                    {
                        QPointF startPoint = m_currentPosition;
                        QPointF endPoint(x, y);
                        QVector<QPointF> centers;
                        centers << m_currentPosition + QPointF(i, j)
                                << m_currentPosition + QPointF(i, -j)
                                << m_currentPosition + QPointF(-i, j)
                                << m_currentPosition + QPointF(-i, -j);
                        qreal startAngle = qSNaN();
                        qreal spanAngle = qSNaN();
                        QRectF rect;
                        for (auto center: centers)
                        {
                            QLineF r1 = QLineF(center, startPoint);
                            QLineF r2 = QLineF(center, endPoint);
                            startAngle = r1.angle();
                            spanAngle = r1.angleTo(r2);
                            qDebug() << "CCW SQ C" << center;
                            qDebug() << "CCW SQ R1" << r1 << r1.angle() << r1.length();
                            qDebug() << "CCW SQ R2" << r2 << r2.angle() << r2.length();
                            qDebug() << "CCW SQ A" << startAngle << spanAngle;
                            if ((360-spanAngle) >= 0 && (360 - spanAngle) <= 90)
                            {
                                if (!qFuzzyCompare(r1.length(), r2.length()))
                                {
                                }
                                qreal radius = r1.length();
                                rect = QRectF(center.x()-radius, center.y()-radius, radius*2.0, radius*2.0);
                                break;
                            }
                        }


                        if (m_inRegion)
                        {
                            if (m_currentRegion.isEmpty())
                                m_currentRegion.moveTo(startPoint);
                            m_currentRegion.arcTo(rect, startAngle, 360-spanAngle);
                        }
                        else
                        {
                            QPainterPath path;
                            path.moveTo(startPoint);
                            path.arcTo(rect, startAngle, 360-spanAngle);
                            auto object = new QGraphicsPathItem; // Pos = [0, 0]
                            object->setPath(path);
                            object->setBrush(Qt::NoBrush);
                            object->setPen(QPen(Qt::darkMagenta, m_currentAperture->width,
                                                Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
                            m_objects.append(object);
                        }

                        m_currentPosition = endPoint;

                    }
                    else if (m_quadrant == MultipleQuadrant)
                    {
                        QPointF startPoint = m_currentPosition;
                        QPointF endPoint(x, y);
                        QPointF center = m_currentPosition + QPointF(i, j);
                        QLineF r1 = QLineF(center, startPoint);
                        QLineF r2 = QLineF(center, endPoint);
                        qreal startAngle = r2.angle();
                        qreal spanAngle = r2.angleTo(r1);
                        if (qFuzzyCompare(spanAngle, 0.0))
                            spanAngle = 360.0;
                        if (!qFuzzyCompare(r1.length(), r2.length()))
                            qDebug() << "CCW MQ" << m_inRegion
                                     << r1 << r1.length()
                                     << r2 << r2.length()
                                     << startAngle << spanAngle;
                        qreal radius = r1.length();
                        QRectF rect(center.x()-radius, center.y()-radius, radius*2.0, radius*2.0);

                        if (m_inRegion)
                        {
                            if (m_currentRegion.isEmpty())
                                m_currentRegion.moveTo(startPoint);
                            m_currentRegion.arcTo(rect, startAngle, spanAngle);
                        }
                        else
                        {
                            QPainterPath path;
                            path.moveTo(startPoint);
                            path.arcTo(rect, startAngle, spanAngle);

                            auto object = new QGraphicsPathItem; // Pos = [0, 0]
                            object->setPath(path);
                            object->setBrush(Qt::NoBrush);
                            object->setPen(QPen(Qt::darkMagenta, m_currentAperture->width,
                                                Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
                            m_objects.append(object);
                        }

                        m_currentPosition = endPoint;
                    }
                    return true;
                }
            }

            return false;
        }
        case MoveCommand::Type: // D02
        {
            if (m_inRegion)
            {
                m_regions.append(m_currentRegion);
                m_currentRegion = QPainterPath();
            }

            auto *cmd = static_cast<const MoveCommand*>(command);
            if (!qIsNaN(cmd->xCoordinate))
                m_currentPosition.setX(cmd->xCoordinate);
            if (!qIsNaN(cmd->yCoordinate))
                m_currentPosition.setY(cmd->yCoordinate);
            //qDebug() << "Current pos" << m_currentPosition;
            return true;
        }
        case FlashCommand::Type:
        {
            auto *cmd = static_cast<const FlashCommand*>(command);
            auto object = new QGraphicsPathItem;
            QPointF pos = QPointF(cmd->xCoordinate, cmd->yCoordinate);
            if (qIsNaN(pos.x()))
                pos.setX(m_currentPosition.x());
            if (qIsNaN(pos.y()))
                pos.setY(m_currentPosition.y());
            object->setPos(pos);
            object->setPath(m_currentAperture->shape);
            object->setBrush(QBrush(Qt::darkBlue));
            object->setPen(QPen(Qt::black, 0));
            m_objects.append(object);
            return true;
        }
        case SelectQuadrantCommand::Type:
        {
            auto *cmd = static_cast<const SelectQuadrantCommand*>(command);
            m_quadrant = cmd->quadrant;
            //qDebug() << "Select quadrant" << m_quadrant;
            return true;
        }
        case BeginRegionCommand::Type:
        {
            m_inRegion = true;
            m_currentRegion = QPainterPath();
            m_regions.clear();
            return true;
        }
        case EndRegionCommand::Type:
        {
            m_regions.append(m_currentRegion);
            for (auto region: m_regions)
            {
                if (region.isEmpty())
                    continue;

                auto object = new QGraphicsPathItem;
                object->setPath(region);
                object->setBrush(QBrush(Qt::darkGreen));
                object->setPen(QPen(Qt::black, 0));
                m_objects.append(object);
            }

            m_inRegion = false;
            m_currentRegion = QPainterPath();
            m_regions.clear();

            return true;
        }
        case EndCommand::Type:
        {
            auto *cmd = static_cast<const EndCommand*>(command);
            break;
        }
        case SelectPolarityCommand::Type:
        {
            qDebug() << "Select polarity";
            auto *cmd = static_cast<const SelectPolarityCommand*>(command);
            m_polarity = cmd->polarity;
            return true;
        }
        case SetFileAttributeCommand::Type:
        {
            auto *cmd = static_cast<const SetFileAttributeCommand*>(command);
            break;
        }
        case SetApertureAttributeCommand::Type:
        {
            auto *cmd = static_cast<const SetApertureAttributeCommand*>(command);
            break;
        }
        case ClearApertureAttributeCommand::Type:
        {
            auto *cmd = static_cast<const ClearApertureAttributeCommand*>(command);
            break;
        }
        case AddApertureCommand::Type:
        {
            auto *cmd = static_cast<const AddApertureCommand*>(command);
            if (!m_apertureTemplateDictionary.contains(cmd->templateName))
            {
                qDebug() << "Aperture template not found" << cmd->templateName;
                return false;
            }
            if (m_apertureDictionary.contains(cmd->apertureNumber))
            {
                qDebug() << "Aperture already defined" << cmd->apertureNumber;
                return false;
            }
            auto aperture = m_apertureTemplateDictionary.value(cmd->templateName)->instanciate(cmd->parameters);
            if (aperture == nullptr)
            {
                qDebug() << "Cannot instanciate aperture" << cmd->apertureNumber << cmd->templateName;
                return false;
            }
            aperture->apertureNumber = cmd->apertureNumber;
            m_apertureDictionary.insert(aperture->apertureNumber, aperture);
            qDebug() << aperture->apertureNumber << aperture->width;
            return true;
        }
        case SelectApertureCommand::Type:
        {
            auto *cmd = static_cast<const SelectApertureCommand*>(command);

            if (!m_apertureDictionary.contains(cmd->apertureNumber))
            {
                qDebug() << "Aperture not found" << cmd->apertureNumber;
                return false;
            }
            m_currentAperture = m_apertureDictionary.value(cmd->apertureNumber);
            return true;
        }
        default:
            qDebug() << "Invalid";
            return false;
    }
    return false;
}

QList<QGraphicsItem *> GerberProcessor::objects() const
{
    return m_objects;
}
