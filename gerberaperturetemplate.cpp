#include "gerberaperturetemplate.h"

#include <QtMath>
#include <QDebug>

GerberApertureTemplate::GerberApertureTemplate(const QString &name, const QString &friendlyName, bool isBuiltIn):
    name(name), friendlyName(friendlyName), isBuiltIn(isBuiltIn)
{

}

GerberApertureTemplate::~GerberApertureTemplate()
{

}


GerberCircleApertureTemplate::GerberCircleApertureTemplate():
    GerberApertureTemplate("C", "Circle", true)
{

}

GerberAperture *GerberCircleApertureTemplate::instanciate(const QList<qreal> parameters) const
{
    if (parameters.count() < 1 || parameters.count() > 2)
    {
        return nullptr;
    }

    auto result = new GerberAperture;

    qreal radius = parameters.at(0)/2.0;
    result->shape.addEllipse(QPointF(0, 0), radius, radius);

    if (parameters.count() >= 2)
    {
        QPainterPath hole;
        qreal holeRadius = parameters.at(1)/2.0;
        hole.addEllipse(QPointF(0, 0), holeRadius, holeRadius);
        result->shape -= hole;
    }

    result->width = 2.0*radius;

    return result;
}

GerberRectApertureTemplate::GerberRectApertureTemplate():
    GerberApertureTemplate("R", "Rectangle", true)
{

}

GerberAperture *GerberRectApertureTemplate::instanciate(const QList<qreal> parameters) const
{
    if (parameters.count() < 2 || parameters.count() > 3)
    {
        return nullptr;
    }

    auto result = new GerberAperture;

    qreal width = parameters.at(0);
    qreal height = parameters.at(1);
    result->shape.addRect(-width/2.0, -height/2.0, width, height);

    if (parameters.count() == 3)
    {
        QPainterPath hole;
        qreal holeRadius = parameters.at(2)/2.0;
        hole.addEllipse(QPointF(0, 0), holeRadius, holeRadius);
        result->shape -= hole;
    }

    result->width = width;

    return result;
}

GerberObroundApertureTemplate::GerberObroundApertureTemplate():
    GerberApertureTemplate("O", "Obround", true)
{
}

GerberAperture *GerberObroundApertureTemplate::instanciate(const QList<qreal> parameters) const
{
    if (parameters.count() < 2 || parameters.count() > 3)
    {
        return nullptr;
    }

    auto result = new GerberAperture;

    qreal width = parameters.at(0);
    qreal height = parameters.at(1);
    result->shape.addRect(-width/2.0, -height/2.0, width, height);

    QPainterPath round;
    qreal radius = qMin(width, height)/2.0;
    round.addEllipse(0, 0, radius, radius);
    if (width > height)
    {
        result->shape += round.translated(width/2.0, 0);
        result->shape += round.translated(-width/2.0, 0);
    }
    else
    {
        result->shape += round.translated(0, height/2.0);
        result->shape += round.translated(0, -height/2.0);
    }

    if (parameters.count() == 3)
    {
        QPainterPath hole;
        qreal holeRadius = parameters.at(2)/2.0;
        hole.addEllipse(QPointF(0, 0), holeRadius, holeRadius);
        result->shape -= hole;
    }

    return result;
}

GerberPolygonApertureTemplate::GerberPolygonApertureTemplate():
    GerberApertureTemplate("P", "Polygon", true)
{

}

GerberAperture *GerberPolygonApertureTemplate::instanciate(const QList<qreal> parameters) const
{
    if (parameters.count() < 2 || parameters.count() > 4)
    {
        return nullptr;
    }

    auto result = new GerberAperture;

    qreal radius = parameters.at(0)/2.0;
    int edges = qRound(qAbs(parameters.at(1)));
    qreal rotation = 0;
    if (parameters.count() >= 3)
        rotation = parameters.at(2) * 10.0;
    QPolygonF polygon;
    for (int i = 0; i < edges; i++)
    {
        qreal alpha = qDegreesToRadians(i*(360.0/edges) + rotation);
        qreal x = radius*qCos(alpha);
        qreal y = radius*qSin(alpha);
        polygon.append(QPointF(x, y));
    }
    result->shape.addPolygon(polygon);
    result->shape.closeSubpath();

    if (parameters.count() == 4)
    {
        QPainterPath hole;
        qreal holeRadius = parameters.at(3)/2.0;
        hole.addEllipse(QPointF(0, 0), holeRadius, holeRadius);
        result->shape -= hole;
    }

    return result;
}
